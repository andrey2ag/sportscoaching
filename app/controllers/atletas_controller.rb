class AtletasController < ApplicationController


  def index
    authorize Atleta
    @atletas = Atleta.all
  end

  def show
    authorize Atleta
    @atleta = Atleta.find(params[:id])

  end

  def new
    authorize Atleta
    @atleta = Atleta.new
  end

  def edit
    authorize Atleta
    @atleta = Atleta.find(params[:id])
  end

  def create
    authorize Atleta
    @atleta = Atleta.new(user_params)

    temporaryPassword = (0...8).map { ('a'..'z').to_a[rand(26)] }.join

    @atleta.password = temporaryPassword
    # @atleta.is_atleta = true

    if @atleta.save
      UserMailer.welcome_email(@atleta).deliver_now
      redirect_to atletas_path, notice: 'El Atleta fue creado satisfactoriamente.'
    else
      render :new
    end
  end

  def update
    authorize Atleta
    @atleta = Atleta.find(params[:id])
    if params[:atleta][:password].blank? && params[:atleta][:password_confirmation].blank?
      params[:atleta].delete(:password)
      params[:atleta].delete(:password_confirmation)
    end

    if @atleta.update_attributes(user_params)
      redirect_to atletas_path, :notice => 'Atleta actualizado.'
    else
      redirect_to atletas_path, :alert => 'No se pudo actualizar el Atleta.'
    end
  end

  def destroy
    authorize Atleta
    atleta = Atleta.find(params[:id])
    atleta.destroy
    redirect_to atletas_path, :notice => 'Atleta eliminado.'
  end

  def generarContrasena
    authorize Atleta
    @atleta = Atleta.find(params[:id])

    temporaryPassword = (0...8).map { ('a'..'z').to_a[rand(26)] }.join
    @atleta.password = temporaryPassword

    if @atleta.save

      UserMailer.forgot_password(@atleta).deliver

      redirect_to atletas_path, :notice => 'Contraseña enviada.'
    else
      redirect_to atletas_path, :notice => @atleta.errors
    end

  end

  private

  def user_params
    params.require(:atleta).permit(:cedula, :nombre, :apellidos, :email)
  end



end
