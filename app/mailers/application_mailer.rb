class ApplicationMailer < ActionMailer::Base
  default from: "noreply@uvitadevs.com"
  layout 'mailer'
end
