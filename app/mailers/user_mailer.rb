class UserMailer < ApplicationMailer

  def welcome_email(atleta)
    @atleta = atleta
    @urlAndroid  = 'https://play.google.com'
    @urliOS = 'https://itunes.apple.com'
    mail(to: @atleta.email, subject: "Bienvenido, #{atleta.name}")
  end

  def forgot_password(atleta)
    @atleta = atleta
    mail(to: @atleta.email, subject: "Recuperación de contraseña - AlexAzo Sports Coaching")
  end

end
