class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  validates :name, presence: true

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  enum role: [:superadmin, :admin, :entrenador]

  after_initialize :set_default_role, :if => :new_record?
  before_create :generate_authentication_token!

  def set_default_role
    self.role ||= :admin
  end

  def generate_authentication_token!
    begin
      self.auth_token = Devise.friendly_token
    end while self.class.exists?(auth_token: auth_token)
  end

  def self.search(params = {})
    users = params[:role].present? ? User.where('role = ?', User.roles[params[:role]]) : User.all
    users
  end

  def name
    "#{self.nombre} #{self.apellido1} #{self.apellido2}"
  end


end
