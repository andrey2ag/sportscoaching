class AddFieldToAtleta < ActiveRecord::Migration
  def change
    add_column :atletas, :nombre, :string
    add_column :atletas, :apellidos, :string
    add_column :atletas, :role, :integer
    add_column :atletas, :auth_token, :string
    add_column :atletas, :cedula, :string

    add_column :atletas, :fecha_nacimiento, :date
    add_column :atletas, :residencia, :string
    add_column :atletas, :genero, :string
    add_column :atletas, :telefono, :string
    add_column :atletas, :ocupacion, :string

    add_column :atletas, :emergencia_nombre, :string
    add_column :atletas, :emergencia_telefono, :string


  end
end
