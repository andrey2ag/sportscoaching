class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :nombre, :string
    add_column :users, :apellido1, :string
    add_column :users, :apellido2, :string
    add_column :users, :role, :integer
    add_column :users, :auth_token, :string
    add_column :users, :cedula, :string

  end
end
