Rails.application.routes.draw do

  # devise_for :atletas
  devise_for :atletas, :skip => [:registrations]
  devise_for :users

  resources :atletas
  get 'home/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

end
